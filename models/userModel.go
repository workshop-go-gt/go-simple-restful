package models

import (
	"go-simple-restful/db"
	"go-simple-restful/forms"
)

//UserModel is the user model
type UserModel struct{}

// GetList is for getting list of users
func (um *UserModel) GetList() (u []forms.User) {
	db.GetDB().
		Table("users").
		Find(&u)
	return
}
