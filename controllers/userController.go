package controllers

import (
	"go-simple-restful/models"

	"github.com/gin-gonic/gin"
)

// UserController is
type UserController struct{}

var userModel = new(models.UserModel)

// List is
func (s *UserController) List(c *gin.Context) {
	userList := userModel.GetList()
	c.JSON(200, gin.H{
		"status":  200,
		"message": "success",
		"data": gin.H{
			"users": userList,
		},
	})
}
