package db

import (
	"fmt"
	"log"

	//import dialect
	"github.com/bxcodec/faker"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"

	"go-simple-restful/forms"

	"github.com/spf13/viper"
)

var db *gorm.DB

// InitMysql is
func InitMysql() {
	//open a mysql connection
	var err error
	db, err = gorm.Open("mysql", viper.GetString("database.mysql.user")+":"+viper.GetString("database.mysql.password")+"@tcp("+viper.GetString("database.mysql.host")+":"+viper.GetString("database.mysql.port")+")/"+viper.GetString("database.mysql.dbname")+"?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		log.Fatal("failed to connect database")
	}
}

// Migrate is for migrate all needed tables
func Migrate() {
	if !db.HasTable("users") {
		db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&forms.User{})
		if viper.GetString("mode") == "development" {
			fmt.Println("Generating fake data")
			for i := 0; i < 3; i++ {
				var a forms.User
				err := faker.FakeData(&a)
				if err != nil {
					fmt.Println(err)
				}
				b := forms.User{Name: a.Name, Email: a.Email, Phone: a.Phone}
				db.Table("users").Create(&b)
			}
		}
	}
}

// GetDB is
func GetDB() *gorm.DB {
	return db
}
