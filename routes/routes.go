package routes

import (
	"go-simple-restful/controllers"

	"github.com/gin-gonic/gin"
)

//DefaultRoute is
type DefaultRoute struct{}

//Init is
func (d *DefaultRoute) Init(router *gin.Engine) {
	uc := new(controllers.UserController)
	router.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "hello there",
		})
	})
	v1 := router.Group("/v1")
	{
		v1.GET("/user", uc.List)
	}
	router.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{
			"status":  404,
			"message": "not found",
		})
	})
}
