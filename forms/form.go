package forms

// User is the data structure for users table
type User struct {
	ID    int    `gorm:"primary_key;column:id_user" json:"-"`
	Name  string `gorm:"column:name" faker:"name" json:"name"`
	Email string `gorm:"column:email" faker:"email" json:"email"`
	Phone string `gorm:"column:phone" faker:"phone_number" json:"phone"`
}
