module go-simple-restful

go 1.12

require (
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/getsentry/sentry-go v0.2.1
	github.com/gin-gonic/gin v1.4.0
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-sql-driver/mysql v1.4.1
	github.com/jinzhu/gorm v1.9.10
	github.com/spf13/viper v1.4.0
	github.com/tidwall/gjson v1.3.2
)
