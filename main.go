package main

import (
	"fmt"

	"go-simple-restful/db"
	"go-simple-restful/routes"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigFile("config.json")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}
	db.InitMysql()
	err = db.GetDB().DB().Ping()
	if err != nil {
		panic(fmt.Errorf("Error ping : %s", err))
	}
	fmt.Println("Migrating tables")
	db.Migrate()
	fmt.Println("All tables is ready")
	defaultRoute := new(routes.DefaultRoute)
	r := gin.Default()
	defaultRoute.Init(r)
	if viper.GetString("mode") == "production" {
		gin.SetMode(gin.ReleaseMode)
	}
	r.Run(viper.GetString("server.host") + ":" + viper.GetString("server.port"))
}
